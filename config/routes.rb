Rails.application.routes.draw do
  mount ActionCable.server => '/query_update'

  resources :users
  resources :queries

  get '/users_queries', to: 'users_queries#index'
  get '/users/:user_id/queries', to: 'users_queries#user_queries'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
