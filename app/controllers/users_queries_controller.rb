class UsersQueriesController < ApplicationController
  def index
    render json: UsersQuery.all
  end

  def user_queries
    render json: UsersQuery.where(user_id: params[:user_id]).select(:query_id).map { |x| Query.find(x.query_id)}
  end
end