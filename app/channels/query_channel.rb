class QueryChannel < ApplicationCable::Channel
  def subscribed
    channel_name = QueryChannel::channel_name_by params['query_id']
    stream_from channel_name
    ActionCable.server.broadcast channel_name, message: params['query_id']
  end

  def unsubscribed
    # cleanup
  end
end