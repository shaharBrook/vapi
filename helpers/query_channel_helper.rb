module QueryChannel
  def channel_name_by id
    "Query_#{id}_channel"
  end
end